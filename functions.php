<?php

function theme_setup(){
	
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('widgets');
	add_theme_support('menus');
	add_theme_support('custom-header');
	add_theme_support('custom-background');
	
}
add_action('after_setup_theme', 'theme_setup');